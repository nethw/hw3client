import com.google.gson.annotations.SerializedName
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

data class Product(
    @SerializedName("text") var text: String? = null,
)

class Client(private val url: String, private val port: String) {

    private val api = provideApi()

    private fun provideApi(): Api {
        return Retrofit.Builder()
            .baseUrl("$url:$port")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api::class.java)
    }

    fun getFile(filename: String?) : String{
        println("trying to find: $filename")
        if (filename == null || filename == "") {
            return "Filename is empty"
        }
        return try {
            val response = api.getTransactions(filename).execute()
            if (response.code() != 200) {
                return "Got: ${response.code()} error code"
            }
            val result = response.body()
            result!!.text!!
        } catch (e: Exception) {
            "${e.message}"
        }
    }
}