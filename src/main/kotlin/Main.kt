fun main(args: Array<String>) {
    if (args.size != 3) {
        var run = true
        val client = Client("http://localhost", "8082")

        while (run) {
            val cmd = readLine()

            if (cmd == "exit") {
                run = false
            }

            println(client.getFile(cmd))
        }
    } else {
        val client = Client(args[0], args[1])

        println(client.getFile(args[2]))
    }
//    println(args[0])
//    println(args[1])
//    println(args[2])
//
//    val client = Client(args[0], args[1])
//
//    println(client.getFile(args[2]))
}