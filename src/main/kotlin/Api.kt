import com.squareup.moshi.Json
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("code/get-file")
    fun getTransactions(@Query("filename") filename: String) : Call<Product>
}